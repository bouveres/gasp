DROP TABLE IF EXISTS InitialCEAllocation;
DROP TABLE IF EXISTS CompetitionState;
DROP TABLE IF EXISTS Agent;
DROP TABLE IF EXISTS Good;
DROP TABLE IF EXISTS SAACompetition;
DROP TABLE IF EXISTS Competition;

CREATE TABLE Competition (
       competitionId VARCHAR(40) PRIMARY KEY,
       title VARCHAR(80),
       description VARCHAR(300),
       responseClock INTEGER,
       bidClock INTEGER,
       mechanism VARCHAR(30),
       currentInstance BYTEA
);

CREATE TABLE SAACompetition (
       competitionId VARCHAR(40) PRIMARY KEY,
       startPrice INT,
       increment INT,
       FOREIGN KEY (competitionId) REFERENCES Competition(competitionId)
);

CREATE TABLE Good (
       competitionId VARCHAR(40),
       goodName VARCHAR(20),
       PRIMARY KEY (competitionId, goodName),
       FOREIGN KEY (competitionId) REFERENCES Competition(competitionId)
);

CREATE TABLE Agent (
       competitionId VARCHAR(40),
       agentName VARCHAR(20),
       url VARCHAR(80),
       valuation TEXT,
       PRIMARY KEY (competitionId, agentName),
       FOREIGN KEY (competitionId) REFERENCES Competition(competitionId)
);

CREATE TABLE CompetitionState (
       competitionId VARCHAR(40),
       agentName VARCHAR(20),
       state VARCHAR(20),
       PRIMARY KEY (competitionId, agentName),
       FOREIGN KEY (competitionId) REFERENCES Competition(competitionId),
       FOREIGN KEY (competitionId, agentName) REFERENCES Agent(competitionId, agentName)
);

CREATE TABLE InitialCEAllocation (
       competitionId VARCHAR(40),
       goodName VARCHAR(20),
       agentName VARCHAR(20),
       quantity INT,
       PRIMARY KEY (competitionId, goodName, agentName),
       FOREIGN KEY (competitionId) REFERENCES Competition(competitionId),
       FOREIGN KEY (competitionId, goodName) REFERENCES Good(competitionId, goodName),
       FOREIGN KEY (competitionId, agentName) REFERENCES Agent(competitionId, agentName)
);
