import json
import coloredlogs
import logging
import traceback
import uuid
from functools import wraps
import gasp_server.db as db

from flask import Flask, request, url_for, current_app
from gasp_server.auction_runner import start_auction, submit_bid,\
    SUPPORTED_AUCTIONS, UnsupportedAuction


WITH_TRACES = True


def with_valid_competition(with_goods=False, with_agents=False):
    def _with_valid_competition_decorator(fn):
        @wraps(fn)  # This is needed to keep the original function's name (used by the flask route)
        def _wrapped(*args, **kwargs):
            competition_id = args[0]
            try:
                cursor = db.execute_query(
                    """
                    SELECT competitionId, title, description, responseClock, bidClock, mechanism
                    FROM Competition
                    WHERE competitionId = %s""",
                    competition_id)
                row = cursor.fetchone()
                cursor.close()
                if row is None:
                    return {"message": "Unknown competition id"}, 404
                competition = {
                    "competition_id": row[0],
                    "title": row[1],
                    "description": row[2],
                    "response_clock": int(row[3]),
                    "bid_clock": int(row[4]),
                    "mechanism": row[5],
                    "url": f'http://{request.host}/{row[0]}'}
                if competition['mechanism'] not in SUPPORTED_AUCTIONS:
                    raise UnsupportedAuction()
                if with_goods:
                    cursor = db.execute_query(
                        "SELECT goodName FROM Good WHERE competitionId = %s",
                        competition_id)
                    competition["goods"] = [row[0] for row in cursor]
                    cursor.close()
                if with_agents:
                    cursor = db.execute_query(
                        "SELECT agentName, url, valuation FROM Agent WHERE competitionId = %s",
                        competition_id)
                    competition["agents"] = [
                        {
                            "id": row[0],
                            "url": row[1],
                            "valuation": json.loads(row[2])
                        }
                        for row in cursor]
                    cursor.close()
                SUPPORTED_AUCTIONS[competition['mechanism']]['load'](competition)
                return fn(competition, *args[1:], **kwargs)
            except Exception as e:
                current_app.logger.warning(repr(e))
                if WITH_TRACES:
                    traceback.print_exc()
                return {"message": f"Bad request: {repr(e)}"
                        if current_app.config['DEBUG'] else "Bad request"}, 400
        return _wrapped

    return _with_valid_competition_decorator


def create_app(logging_level=logging.WARNING, config='gasp_server.config.DevConfig'):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(config)
    coloredlogs.install(logger=app.logger, level='DEBUG')

    from . import db
    db.init_app(app)

    @app.route('/competitions', methods=['GET', 'POST'])
    def competitions():
        if request.method == 'POST':
            return new_competition()
        return competition_list()

    @app.route('/<uuid:competition_id>', methods=['GET', 'DELETE'])
    def competition(competition_id):
        if request.method == 'DELETE':
            return delete_competition(str(competition_id))
        return competition_info(str(competition_id))

    @app.route('/<uuid:competition_id>/start', methods=['GET'])
    def start(competition_id):
        return start_competition(str(competition_id))

    @app.route('/<uuid:competition_id>/<agent_id>/bid', methods=['POST'])
    def bid(competition_id, agent_id):
        return send_bid(str(competition_id), agent_id)

    return app


def new_competition():
    content = request.get_json()
    new_id = uuid.uuid4()
    try:
        if content["mechanism"] not in SUPPORTED_AUCTIONS:
            raise UnsupportedAuction()
        content["competition_id"] = str(new_id)
        cursor = db.execute_query(
            """
            INSERT INTO Competition (
                competitionId, title, description, responseClock, bidClock, mechanism
            ) VALUES (%s, %s, %s, %s, %s, %s)""",
            content["competition_id"],
            content["title"],
            content["description"],
            content["response_clock"],
            content["bid_clock"],
            content["mechanism"])
        cursor.close()
        for good in content["goods"]:
            cursor = db.execute_query(
                """
                INSERT INTO Good (
                    competitionId, goodName
                ) VALUES (%s, %s)""",
                content["competition_id"],
                good)
            cursor.close()
        for agent in content["agents"]:
            cursor = db.execute_query(
                """
                INSERT INTO Agent (
                    competitionId, agentName, url, valuation
                ) VALUES (%s, %s, %s, %s)""",
                content["competition_id"],
                agent["id"],
                agent["url"],
                json.dumps(agent["valuation"]))
            cursor.close()
        SUPPORTED_AUCTIONS[content['mechanism']]['create'](content)
        db.commit()
    except Exception as e:
        current_app.logger.warning(e)
        if WITH_TRACES:
            traceback.print_exc()
        return {"message": f"Bad request: {e}"
                if current_app.config['DEBUG'] else "Bad request"}, 400
    return {"message": "Competition created",
            "url": "http://" + request.host + url_for("competition", competition_id=new_id)}


def competition_list():
    cursor = db.execute_query(
        """
        SELECT title, description, mechanism, COUNT(DISTINCT goodName), COUNT(DISTINCT agentName)
        FROM Competition NATURAL JOIN Good NATURAL Join Agent
        GROUP BY competitionId, title, description, mechanism""")
    response = [
        {"title": row[0],
         "description": row[1],
         "mechanism": row[2],
         "nb_goods": int(row[3]),
         "nb_agents": int(row[4])
         } for row in cursor]
    cursor.close()
    return response


@with_valid_competition(with_goods=False, with_agents=False)
def delete_competition(competition):
    SUPPORTED_AUCTIONS[competition['mechanism']]['delete'](competition)
    competition_id = competition['competition_id']
    cursor = db.execute_query(
        """
        DELETE FROM Agent
        WHERE competitionId = %s""",
        competition_id)
    cursor.close()
    cursor = db.execute_query(
        """
        DELETE FROM Good
        WHERE competitionId = %s""",
        competition_id)
    cursor.close()
    cursor = db.execute_query(
        """
        DELETE FROM Competition
        WHERE competitionId = %s""",
        competition_id)
    db.commit()
    if cursor.rowcount == 1:
        cursor.close()
        return {"message": "Competition deleted"}
    cursor.close()
    return {"message": "Unknown competition id"}, 404


@with_valid_competition(with_goods=True, with_agents=True)
def competition_info(competition):
    return competition


@with_valid_competition(with_goods=True, with_agents=True)
def start_competition(competition):
    try:
        start_auction(competition)
    except Exception as e:
        current_app.logger.warning(e)
        if WITH_TRACES:
            traceback.print_exc()
        return {"message": f"Error: {e}"}
    return {"message": "Competition started"}


@with_valid_competition(with_goods=True, with_agents=True)
def send_bid(competition, agent_id):
    content = request.get_json()
    bid = content['bid']
    current_app.logger.debug(f'Bid received from {agent_id}: {bid}')
    try:
        submit_bid(competition, agent_id, bid)
    except Exception as e:
        current_app.logger.warning(e)
        if WITH_TRACES:
            traceback.print_exc()
        return {"message": f"Error: {e}"}
    return {"message": "Bid submitted"}
